import os

# execution mode (MAIN, DEBUG)
EXEC_MODE = "MAIN"
# EXEC_MODE = "DEBUG"

# scraper to use (GF_WEB)
SCRAPER = "GF_WEB"

# use browser (PHANTOMJS, FIREFOX)
BROWSER = "FIREFOX"
# BROWSER = "PHANTOMJS"

# browser paths
PHANTOMJS_PATH = r"C:\Users\Andreas\Programming\Webbrowsers\phantomjs.exe"
FIREFOX_PATH = r"C:\Users\Andreas\Programming\Webbrowsers\geckodriver.exe"

# Browser visibility
BROWSER_VISIBLE = 1

# main directory
MAIN_DIR = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

# log files
LOG_FILE = os.path.join(MAIN_DIR, "logs", "info.log")
MAX_BYTES = 2*1024*1024  # 2 MB
PHANTOMJS_LOG = os.path.join(MAIN_DIR, "logs", "phantomjs_log")
FIREFOX_LOG = os.path.join(MAIN_DIR, "logs", "geckodriver_log")

# site loading delay
SITE_LOAD_DELAY = 2
GOOGLE_REQUEST_MAX_TIME_WAIT = 2000  # ms
