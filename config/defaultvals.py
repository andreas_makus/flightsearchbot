# default values
AIRPORTS = r"C:\Users\Andreas\Programming\FlightSearchBot\data\airport_connections.txt"  # default file to read from
WEEKDAY_DEP = 4  # default weekday departure, counting from Monday = 0
WEEKDAY_RET = 6  # default weekday return, counting from Monday = 0
WEEKS_AHEAD = 2  # weeks ahead
MAX_PRICE = 250
FLIGHTS_PER_SEARCH = 1