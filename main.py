"""
This is the main file for running the flight search bot.

Author: Andreas Makus
"""
import sys
import logging
import argparse
import calendar

from config import config, defaultvals, log

from sample.googleflights import GoogleFlights
from sample import tools
from sample import analyze as an
from sample import email


def main(airports_file, weekday_departure, weekday_return, weeks_ahead, sender, password, receivers):
    """"""

    # initiate logger
    logger = log.configure_logger(logging.getLogger(__name__))
    log.log_blank_line()
    logger.info("Script initiated")

    # load file with airports
    try:
        logger.info("Reading airport connections from: %s", airports_file)
        airports = tools.get_airport_connections_from_file(airports_file)
    except IOError as err:
        logger.exception(err)
        logger.info("Exiting script early")
        sys.exit()

    # get dates for which to perform search

    logger.info("Calculating flight dates for the next %s weeks for weekdays %s (departure) and %s (return)",
                weeks_ahead, calendar.day_name[weekday_departure], calendar.day_name[weekday_return])
    dates = tools.get_flight_dates(weekday_departure, weekday_return, weeks_ahead)

    # use scraper to get site info
    if config.SCRAPER == "GF_WEB":
        logger.info("Using Google Flights Web Search for scraping")
        gf = GoogleFlights(airports, dates)
        scraped_flights = gf.scrape_flights()
        search_engine = "Google Flights Web Search"
    else:
        scraped_flights = []
        search_engine = None

    # check if any flights were scraped
    try:
        scraped_flights[0]
    except IndexError:
        logger.error("No flights were scraped")
        logger.info("Exiting script early")
        sys.exit()
    else:
        logger.info("%s flights collected", len(scraped_flights))


    # group by flights by dates, sort by price
    flights_by_date = an.group_by_dates(scraped_flights, dates)
    for date, flights_list in flights_by_date.items():
         flights_list = an.sort_list_of_dicts_by(flights_list, by="price")

    # start mailer and send email
    mailer = email.Mailer(sender, password, receivers)
    mailer.send_email(search_engine, flights_by_date)

    logger.info("Exiting script normally")
    return


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-airports_file", required=False, default=defaultvals.AIRPORTS)
    parser.add_argument("-weekday_departure", required=False, default=defaultvals.WEEKDAY_DEP)
    parser.add_argument("-weekday_return", required=False, default=defaultvals.WEEKDAY_RET)
    parser.add_argument("-weeks_ahead", required=False, default=defaultvals.WEEKS_AHEAD)
    parser.add_argument("-max_price", required=False, default=defaultvals.MAX_PRICE)
    parser.add_argument("-sender", required=True)
    parser.add_argument("-password", required=True)
    parser.add_argument("-receivers", required=True, nargs = "+")
    args = parser.parse_args()

    main(airports_file=args.airports_file,
         weekday_departure=args.weekday_departure,
         weekday_return=args.weekday_return,
         weeks_ahead=args.weeks_ahead,
         sender=args.sender,
         password=args.password,
         receivers=args.receivers
         )


# TODO: script should also work for indicated dates/list of dates
# TODO: create local database to be able to compare prices, find average, etc