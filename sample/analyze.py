import json

def make_list_of_dicts_unique(li):
    """
    Returns list with unique elements.

    :param list:
    :return:
    """
    # convert dicts to json strings, check for uniqueness using set, convert back to dict
    dict_strings = list(set(json.dumps(x, sort_keys=True) for x in li))
    unique_list = [json.loads(x) for x in dict_strings]

    return unique_list


def sort_list_of_dicts_by(unsorted, by="price"):
    """
    Sorts list of dicts by provided key.

    :param unsorted:
    :param by:
    :return:
    """
    return sorted(unsorted, key=lambda k: k[by])


def group_by_dates(scraped_flights, dates):
    """"""
    # sort dates by departure date
    sorted_dates = sorted(dates, key=lambda tup: tup[0])

    # group flights by departure date
    flights_by_date = {}
    for date_tpl in sorted_dates:
        flights_by_date[date_tpl] = []
        for flight in scraped_flights:
            if flight["departure date"] == date_tpl[0] and flight["return date"] == date_tpl[1]:
                flights_by_date[date_tpl].append(flight)
        else:
            if not flights_by_date[date_tpl]:  # if no flights on this date, delete
                flights_by_date.pop(date_tpl, None)

    return flights_by_date
