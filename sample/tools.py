import datetime
import csv

from bs4 import BeautifulSoup

from config import config



def html_str_to_soup(html_string):
    """
    This function parses the html string using BeautifulSoup and returns a soup object.

    :param html_string: str
    :return:
    """
    return BeautifulSoup(html_string, "html.parser")


def get_airport_connections_from_file(file_path):
    with open(file_path) as in_file:
        data = csv.reader(in_file, delimiter=' ')
        airport_conns = [tuple(line) for line in data]

    return airport_conns


def get_flight_dates(weekday_departure, weekday_return, weeks_ahead):
    """
    Collects the departure and return dates for the indicated coming weeks in a list of tuples.

    :param day_of_departure:
    :param day_of_return:
    :param weeks_ahead:
    :return:
    """

    flight_dates = []
    date = datetime.date.today()
    week_count = 0

    while week_count <= weeks_ahead:
        date = get_next_day(date, weekday_departure)
        dep = str(date)
        date = get_next_day(date, weekday_return)
        ret = str(date)
        flight_dates.append((dep, ret))
        week_count += 1

    return flight_dates


def get_next_day(date, day=None):
    """
    Returns the next date corresponding to the provided day of the week.

    :param date:
    :param day:
    :return:
    """
    if day:
        while date.weekday() != day:
            date += datetime.timedelta(1)
    else:
        date += datetime.timedelta(1)

    return date

def load_html_file(path_to_file):
    """
    This function parses file contents to an ElementTree object and returns its root.

    :param path_to_file:
    :return:
    """

    with open(path_to_file, "r", encoding="utf-8") as f:
        content = f.read()
        f.close()

    return content

