import smtplib
import logging
import sys
from config import config, log
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class Mailer():
    """
    Class that encompasses all functions necessary to send the parsed html content via email.
    When using your google account to send emails, the "insecure apps" permission must be enabled.

    """
    def __init__(self, sender, password, receivers):
        self.receivers = receivers
        self.sender = sender
        self.password = password
        self.logger = log.configure_logger(logging.getLogger(__name__))

        # log in with email credentials
        self.login_gmail()

    def login_gmail(self):
        """
        Logs in with provided credentials.

        :return:
        """
        try:
            self.mail = smtplib.SMTP('smtp.gmail.com', 587)
            self.mail.ehlo()
            self.mail.starttls()
            self.mail.login(self.sender, self.password)
        except smtplib.SMTPAuthenticationError as err:
            self.logger.error("Could not log into GMAIL using the provided credentials.")
            self.logger.info("Exiting script early")
            sys.exit()

        self.logger.info("Login with email successful")
        return


    def send_email(self, search_engine, flights_by_date):
        """"""

        # https://stackoverflow.com/questions/882712/sending-html-email-using-python
        self.logger.info("Preparing email")

        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = "Automated Job Search"
        msg['From'] = self.sender
        msg['To'] = ", ".join(self.receivers)

        # Create the body of the message (a plain-text and an HTML version).
        html = self.create_email_body(search_engine, flights_by_date)

        # Record the MIME types of both parts - text/plain and text/html.
        mime_text = MIMEText(html, 'html')

        # Attach parts into message container
        msg.attach(mime_text)

        # send email
        try:
            self.mail.sendmail(self.sender, ", ".join(self.receivers), msg.as_string())
        except:
            self.logger.exception("Email could not be sent to %s", self.receivers)
        else:
            self.logger.info("Email was sent successfully to %s", self.receivers)
        finally:
            self.mail.quit()

        return


    def create_email_body(self, search_engine, flights_by_date):
        """
        Creates email body from a list of flights, grouped by date.

        :param body_information:
        :return:
        """

        # create email head
        html_doc = ""
        html_doc += self.create_html_head()  # contains page style

        # create email body
        html_doc += "<body>"
        html_doc += self.create_html_info(search_engine)  # contains description line
        html_doc += self.create_html_content(flights_by_date)

        html_doc += "</body></html>"

        self.logger.debug("Email body created successfully")
        return html_doc


    @staticmethod
    def create_html_head():
        """
        Generates email head containing style options.

        :return:
        """
        head = """
            <html>
                <head>
                <style>
                table {
                    font-family: arial, sans-serif;
                    border-collapse: collapse;
                    width: 80%;
                }
                
                td, th {
                    border: 1px solid #dddddd;
                    text-align: left;
                    padding: 8px;
                }
                
                tr:nth-child(even) {
                    background-color: #dddddd;
                }
                </style>
            </head>
        """
        return head


    @staticmethod
    def create_html_info(search_engine):
        """
        Returns html string with info on which websites have been expected.

        :param websites:
        :return:
        """
        return f"<p> The following flights were found using: {search_engine}</p>"


    def create_html_content(self, flights_by_date):
        """"""
        # departure - return
        # price origin destination departure stops duration airline link
        content = ""
        for date_tpl, flights_list in flights_by_date.items():
            content += f"<h2> {date_tpl[0]} - {date_tpl[1]}: </h2>"
            content += self.get_flights_table_html(flights_list)

        return content


    def get_flights_table_html(self, flights_list):
        tab = "<table>"

        # header cells
        tab += """
            <tr>
                <th>Price</th>
                <th>Orig</th>
                <th>Dest</th>
                <th>Dep</th>
                <th>Stops</th>
                <th>Dur</th>
                <th>Airline</th>
                <th>Link</th>
            </tr>"""

        # loop flights, add info to table row
        for flight in flights_list:
            tab +=  "<tr>" \
                    f"<td>{flight.get('price', '-')}</td>" \
                    f"<td>{flight.get('origin', '-')}</td>" \
                    f"<td>{flight.get('destination', '-')}</td>" \
                    f"<td>{flight.get('departure time', '-')}</td>" \
                    f"<td>{flight.get('stops', '-')}</td>" \
                    f"<td>{flight.get('duration', '-')}</td>" \
                    f"<td>{flight.get('airline', '-')}</td>" \
                    f"<td><a href={flight.get('url', '-')}>link</a></td>" \
                    "</tr>"

        tab += "</table>"
        return tab


    @staticmethod
    def dict_to_html_table(info_dict):
        """
        Takes a dictionary with all relevant job info and turns it into an html-table. Table is returned as string.

        :param info_dict:
        :return:
        """
        job_table = """<table frame="box">"""
        for key, value in info_dict.items():
            if key == "url":
                job_table += f"<tr><td width=20%><b>{key.upper()}</b></td><a href={value}>link</a><td></td></tr>"
            else:
                job_table += f"<tr><td width=20%><b>{key.capitalize()}</b></td>{value}<td></td></tr>"
        job_table += "</table></br>"

        return job_table