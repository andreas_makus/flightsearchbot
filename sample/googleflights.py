import logging
import sys
import re
from time import sleep
from random import randint

from config import config, log, defaultvals
from sample import tools

from selenium import webdriver
from selenium.common.exceptions import WebDriverException




class GoogleFlights():
    """"""

    def __init__(self, airports, dates):
        self.logger = log.configure_logger(logging.getLogger(__name__))
        self.airports = airports
        self.dates = dates
        if config.EXEC_MODE == "MAIN":
            self.start_browser()


    def scrape_flights(self, dep_time=None, ret_time=None, price=None, max_res=None):
        """
        Main method to scrape flights. Scraping loops through dates (outer) and connections (inner) and makes a call
        to Google Flights for each combination. For each combination, the proper URL for the call is assembled. The best
        X amount of flights are recorded for each request and scraped. The data is returned in a list of dicts, where
        each dict contains data for a single flight.

        :param dep_time:
        :param ret_time:
        :param price:
        :param max_res:
        :return:
        """

        # loop through dates, flights to check for best flights per weekend
        flights = []
        for date in self.dates:
            for conn in self.airports:

                # create and load url, get html
                self.logger.info("Creating URL for %s and %s", conn, date)
                url = self.create_search_url(*conn, *date, price=price)

                if config.EXEC_MODE == "MAIN":
                    try:
                        self.browser.get(url)
                        sleep(config.SITE_LOAD_DELAY)
                        html = self.browser.execute_script("return document.body.outerHTML")
                        self.logger.debug("HTML successfully returned")
                    except WebDriverException as err:
                        self.logger.error(err)

                elif config.EXEC_MODE == "DEBUG":
                    # load html file with sample data
                    html = tools.load_html_file(r"C:\Users\Andreas\Programming\FlightSearchBot\tests\gf_test_1.html")

                # scrape html
                self.logger.debug("Scraping HTML")
                scraped = self.scrape_html(html)

                # make sure all values are strings
                for res in scraped:
                    for k, v in res.items():
                        v = str(v)

                # append flight data
                for res in scraped:
                    res["origin"] = conn[0]
                    res["destination"] = conn[1]
                    res["departure date"] = date[0]
                    res["return date"] = date[1]
                    res["url"] = url
                    flights.append(res)

                # sleep for random amount of time
                t = (randint(1,config.GOOGLE_REQUEST_MAX_TIME_WAIT))/1000  # sleep time in sec
                sleep(t)

        self.logger.info("Finished scraping flights")
        self.browser.quit()
        try:
            self.logger.info("Browser closed")
        except AttributeError:  # if debug mode, no self.browser exists
            pass
        return flights


    def scrape_html(self, html):
        """
        Convert to html to soup and scrape flight information.

        :param html:
        :return:
        """

        # HARDCODE
        BASE_ELEM = "div"
        BASE_STRING = "Beste Flüge"

        # load html string into soup
        soup = tools.html_str_to_soup(html)

        # get base element (elem that is used as starting point for searching flight tags)
        base_elem = self.get_html_elem_by_string_content(soup, BASE_ELEM, BASE_STRING)

        # loop through html and add flight details to dict; append dict to list
        flights = []
        for n, flight in enumerate(self.get_flight_elements_html(base_elem)):

            # append flight details, limit flights per search
            if n >= defaultvals.FLIGHTS_PER_SEARCH:
                break
            else:
                flight_data = self.get_flight_data(flight)
                if flight_data:
                    flights.append(flight_data)

        return flights


    def get_flight_elements_html(self, element):
        """
        Yields elements that contain flight information.

        :param element:
        :return:
        """
        for sibling in element.next_siblings:
            if self.is_flight(sibling):
                yield sibling


    def get_flight_data(self, element):
        """
        Retrieve flight data from corresponding html element. This is where changes to html structure must be adapted.

        :param element:
        :return: flight data: dict
        """
        # HARDCODE
        CLS_PRICE = "DQX2Q1B-d-Ab"
        CLS_DURATION = "DQX2Q1B-d-E"
        CLS_STOPS = "DQX2Q1B-d-Qb"
        CLS_DEP_TIME = "DQX2Q1B-d-Zb"
        CLS_AIRLINE = "DQX2Q1B-d-j"

        flight_data = {}

        # get flight price
        try:
            flight_data["price"] = element.find("div", {"class":CLS_PRICE}).string
        except AttributeError: pass

        # get amount of stops
        try:
            flight_data["stops"] = element.find("div", {"class": CLS_STOPS}).string
        except AttributeError: pass

        # get departure time
        try:
            flight_data["departure time"] = element.find("div", {"class": CLS_DEP_TIME}).find("span").string
        except AttributeError: pass

        # get flight duration
        try:
            flight_data["duration"] = element.find("div", {"class": CLS_DURATION}).string
        except AttributeError: pass

        # get airline
        try:
            al = element.find("div", {"class": CLS_AIRLINE}).find("span").string
            if al:
                flight_data["airline"] = al
            else:
                e = element.find("div", {"class": CLS_AIRLINE}).find("div")
                flight_data["airline"] = e.contents[0]
        except AttributeError: pass

        return flight_data



    @staticmethod
    def is_flight(element):
        """
        Checks whether the provided html element is likely a flight posting. At the moment, if the element contains a
        link element and amount of descendants, it will be considered a flight element. This may change according to
        Google's html changes.

        :return:
        """
        # HARDCODE
        DESC = 10  # amount of descendants in element (check if complex element)

        # conditions
        try:
            a_tag = element.findAll("a")  # element has link
        except AttributeError:
            a_tag = False

        try:
            desc = len(list(element.descendants)) > DESC  # certain amount of descendants
        except AttributeError:
            desc = False

        return a_tag and desc


    @staticmethod
    def create_search_url(orig, dest, dep, ret, dtime=None, rtime=None, price=None):
        """
        Creates the url used to retrieve flight information.

        :param orig:
        :param dest:
        :param dep:
        :param ret:
        :param dtime:
        :param rtime:
        :param price:
        :return:
        """

        # https://www.google.de/flights/#search;f=HAM;t=MUC;d=2017-10-27;r=2017-10-29;ti=t1300-2400,t0000-2000;mp=1850
        base = "https://www.google.de/flights/#search;"
        url = base + f"f={orig};" + f"t={dest};" + f"d={dep};" + f"r={ret};"

        # add optional arguments
        if dtime:
            url = url + f"ti=t{dtime};"
        if rtime:
            url = url + f"ti=t{rtime};"
        if price:
            url = url + f"mp={price}"

        return url

    @staticmethod
    def get_html_elem_by_string_content(soup, tag, string):
        """"""
        for div in soup.findAll(tag):
            if div.contents:
                if string in div.contents:
                    return div


    def start_browser(self):
        """
        Initiates browser to scrape web data. Config file determines which browser is used.

        :return:
        """
        try:
            if config.BROWSER == "FIREFOX":
                self.browser = webdriver.Firefox(executable_path=config.FIREFOX_PATH, log_path=config.FIREFOX_LOG)
            else:
                self.browser = webdriver.PhantomJS(config.PHANTOMJS_PATH, service_log_path=config.PHANTOMJS_LOG)
        except:
            pass


        # if not visible, move window; does not apply to headless browsers
        if not config.BROWSER_VISIBLE:
            self.browser.set_window_position(-3000, 0)

        self.logger.info("Browser running: %s", config.BROWSER)
        return
