
This program helps the search for cheap flights and notifies the user by email of any flights that are priced below
average (not yet implemented).

The workflow is a follows:

  * Initiate script using a set of options
  * Select engine: Google Flights
  * Log in with GMAIL account
  * Get the dates for the coming weekends
  * Get airport connections from list of airports (from CVS file)
  * Send request for given airports, dates using proper URL
  * Scrape HTML
  * Analyze results
  * Send email with flights grouped by date of departure

Example use:
~~~
>> python path/to/main.py -airports_file {AIRPORTS} -sender {SENDER} -password {PASSWORD} -receivers {RECEIVERS}
~~~

Inputs to the main file are (defaults can be found in defaultvals.py):

  * airports_file      : path to the file where origin and destination airports are stored (in pairs)
  * weekday_departure  : ranges from 0 (Monday) to 6 (Sunday); defines what day of the week the departure is
  * weekday_return     : ranges from 0 (Monday) to 6 (Sunday); defines what day of the week the return flight is
  * weeks_ahead        : defines how many weeks ahead to check for
  * max_price          : maximum price of the flight
  * sender             : Gmail account of the sender bot
  * password           : password to said account
  * receivers          : list of email addresses (str)
